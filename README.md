# Grafana

This repository contains a `docker-compose.yml` file to create five containers at once:

# Features
* monitor machine data
* monitor machine state

# What's inside?

* [Grafana Server](https://grafana.com/docs/grafana/latest/setup-grafana/start-restart-grafana/)
* [Prometheus Server]()
* [FabAccess Prometheus Exporter](https://gitlab.com/fabinfra/fabaccess/prometheus-exporter)
    * export BFFH machine states in a prometheus metric
* [MQTT Exporter](https://github.com/kpetremann/mqtt-exporter) for Shelly
    * export Shelly states in a prometheus metric
* [MQTT Exporter](https://github.com/kpetremann/mqtt-exporter) for FabDetect

# How to install?
* Install Docker or Podman
* `cd` to the directory where your `docker-compose.yml` is located
* the use `docker-compose up` or `podman-composeup` to create the containers

# About / documentation
With this setup you may create a basic collection of your FabAccess metrics and to add some monitoring and alerting with visual dashboards by Grafana.

You will find more information about [Monitoring at the docs](https://docs.fab-access.org/books/plugins-und-schnittstellen/page/monitoring-prometheus-grafana).